@if (flash()->message)
 <div class="alert alert-{{ flash()->class ?? 'success' }}" role="alert">
    @switch(flash()->class)
        @case('danger')
            <i class="bi bi-exclamation-triangle-fill me-2"></i>
            @break
        @case('warning')
            <i class="bi bi-exclamation-triangle-fill me-2"></i>
            @break
        @default
            <i class="bi bi-check-circle-fill me-2"></i>
    @endswitch

    {{ flash()->message }}
</div>
@endif