<div>
    <x-flash-message />
    <form wire:submit='save'>
        <div class="mb-2">
            <label for="body" class="form-label">Body</label>
            <textarea placeholder="Apa yang sedang anda pikirkan ?" wire:model="form.body" id="body" rows="3" class="form-control @error('form.body') is-invalid @enderror"></textarea>
            @error('form.body')
                <small class="text-danger d-block mt-2">{{ $message }}</small>
            @enderror
        </div>
        <div class="d-flex justify-content-end">
            <button class="btn btn-primary">Save</button>
        </div>
    </form>
</div>
