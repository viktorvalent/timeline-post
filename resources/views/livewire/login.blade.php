<div>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Login</h5>
            <form wire:submit="login">
                <div class="mb-4">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" wire:model="email" id="email"
                        class="form-control @error('email') is-invalid @enderror" />
                    @error('email')
                    <small class="text-danger d-block mt-2">{{ $message }}</small>
                    @enderror
                </div>
                <div class="mb-4">
                    <label for="password" class="form-label">Email</label>
                    <input type="password" wire:model="password" id="password"
                        class="form-control @error('password') is-invalid @enderror" />
                    @error('password')
                    <small class="text-danger d-block mt-2">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
            </form>
        </div>
    </div>
</div>
