<?php

namespace App\Livewire\Forms;

use Livewire\Form;
use Livewire\Attributes\Rule;

class PostForm extends Form
{
    #[Rule(['required'])]
    public string $body = '';

    public function store()
    {
        $post = auth()->user()->posts()->create($this->validate());

        flash('Post created successfully.', 'info');

        $this->reset();

        return $post;
    }
}
